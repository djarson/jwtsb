package com.example.david.Auth;

import com.example.david.Jwt.JwtServices;
import com.example.david.User.Role;
import com.example.david.User.User;
import com.example.david.User.UserRespository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final JwtServices jwtServices;
    private final UserRespository userRespository;
    public AuthResponse Login(LoguinRequest request){
        return null;
    }

    public AuthResponse register(RegisterRequest request){
        User user=User.builder()
                .username(request.getUsername())
                .password(request.getPassword())
                .firtsname(request.getFirstname())
                .lastname(request.getLastname())
                .contry(request.getCountry())
                .role(Role.USER)
                .build();

        userRespository.save(user);

        return AuthResponse.builder()
                .token(jwtServices.getToken(user))
                .build();

    }
}
